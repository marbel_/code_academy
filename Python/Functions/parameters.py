########### Parameters and Arguments ################################

def power(base, exponent):  # Add your parameters here!
  result = base ** exponent
  print(str(base) + " to the power of " + str(exponent) + " is " + str(result) + ".")

power(37, 4)  # Add your arguments here!